import {NavigationContainer} from '@react-navigation/native';
import React from 'react';
import styled from 'styled-components/native';
import {HomeScreen, MapScreen} from './screens';
import {createStackNavigator} from '@react-navigation/stack';
import {InfoModal} from './components/modal';

const Stack = createStackNavigator();
const counter = 0;

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Group>
          <Stack.Screen
            name="Home"
            options={{
              headerBackTitleVisible: false,
            }}
            component={HomeScreen}
            initialParams={{counter}}
          />
          <Stack.Screen
            name="Maps"
            options={{
              headerBackTitleVisible: false,
            }}
            component={MapScreen}
          />
        </Stack.Group>
        <Stack.Group screenOptions={{presentation: 'modal'}}>
          <Stack.Screen
            name="InfoModal"
            component={InfoModal}
            options={{
              headerBackTitleVisible: false,
            }}
          />
        </Stack.Group>
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
