let errorCounter = 0;

const makeRequest = (): Promise<void> => {
  return new Promise((resolve, reject) => {
    errorCounter++;
    if (errorCounter % 5 === 0) {
      reject(new Error('Error cada 5 peticiónes'));
    } else {
      resolve();
    }
  });
};

export {makeRequest};
