import {makeRequest} from '.';

describe('makeRequest function', () => {
  it('should resolve the Promise if errorCounter is not divisible by 5', async () => {
    const expected = undefined;
    let errorCounter = 4;

    const result = await makeRequest();
    expect(errorCounter).toEqual(4);
    expect(result).toEqual(expected);
  });

  it('should reject Promise with an error message if called 5 times', async () => {
    try {
      await Promise.all([
        makeRequest(),
        makeRequest(),
        makeRequest(),
        makeRequest(),
        makeRequest(),
      ]);
    } catch (err) {
      expect(err).toStrictEqual(new Error('Error cada 5 peticiónes'));
    }
  });
});
