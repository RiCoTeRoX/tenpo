import React from 'react';
import renderer from 'react-test-renderer';
import Widgets from './Widgets';

describe('Jest Snapshot testing suite', () => {
  it('Matches DOM Snapshot', () => {
    const widgets = renderer.create(<Widgets />).toJSON();
    expect(widgets).toMatchSnapshot();
  });
});
