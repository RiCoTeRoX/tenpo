import React from 'react';
import styled from 'styled-components/native';

const Widgets = styled.View({
  backgroundColor: '#fff',
  borderTopLeftRadius: 30,
  borderTopRightRadius: 30,
  paddingTop: 30,
  paddingHorizontal: 20,
  height: '100%',
});

export default Widgets;
