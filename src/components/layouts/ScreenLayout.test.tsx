import React from 'react';
import {Text} from 'react-native';
import renderer from 'react-test-renderer';
import ScreenLayout from './ScreenLayout';

describe('Jest Snapshot testing suite', () => {
  it('Matches DOM Snapshot', () => {
    const screenLayout = renderer
      .create(
        <ScreenLayout>
          <Text>Hola</Text>
        </ScreenLayout>,
      )
      .toJSON();
    expect(screenLayout).toMatchSnapshot();
  });
});
