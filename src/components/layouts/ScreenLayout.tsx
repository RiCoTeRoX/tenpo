import React, {PropsWithChildren, FC} from 'react';
import PropTypes from 'prop-types';
import {SafeAreaView, ScrollView, StyleSheet} from 'react-native';

const ScreenLayout: FC<PropsWithChildren<{}>> = ({children}) => {
  return (
    <SafeAreaView style={styles.safeArea}>
      <ScrollView>{children}</ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    backgroundColor: '#f2f2f2',
    flex: 1,
    height: '100%',
  },
});

ScreenLayout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default ScreenLayout;
