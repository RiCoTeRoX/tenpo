import {Button, Text, View} from 'react-native';
import React from 'react';
import styled from 'styled-components/native';

const InfoModal = ({navigation}) => {
  const Container = styled.View({
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  });

  const Title = styled.Text({
    fontSize: 20,
  });

  return (
    <Container>
      <Title>{'Ocurrio un problema de conexión,\n volve a intentarlo.'}</Title>
      <Button onPress={() => navigation.goBack()} title="Cerrar" />
    </Container>
  );
};

export default InfoModal;
