import React from 'react';
import renderer from 'react-test-renderer';
import InfoModal from './InfoModal';

describe('Jest Snapshot testing suite', () => {
  it('Matches DOM Snapshot', () => {
    const infoModal = renderer.create(<InfoModal navigation={{}} />).toJSON();
    expect(infoModal).toMatchSnapshot();
  });
});
