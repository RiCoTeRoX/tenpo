import React from 'react';
import renderer from 'react-test-renderer';
import MapInput from './MapInput';

describe('Jest Snapshot testing suite', () => {
  it('Matches DOM Snapshot', () => {
    const mapInput = renderer
      .create(<MapInput onLocationChange={() => jest.fn()} />)
      .toJSON();
    expect(mapInput).toMatchSnapshot();
  });
});
