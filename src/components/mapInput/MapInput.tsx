import React, {FC} from 'react';
import {
  GooglePlacesAutocomplete,
  Point,
} from 'react-native-google-places-autocomplete';

type MapInputProps = {
  onLocationChange: (location: Point | undefined) => void;
};

const MapInput: FC<MapInputProps> = ({onLocationChange}) => {
  return (
    <GooglePlacesAutocomplete
      placeholder="Search"
      minLength={2}
      listViewDisplayed="auto"
      fetchDetails={true}
      onPress={(data, details = null) => {
        onLocationChange(details?.geometry.location);
      }}
      onFail={error => console.log(error)}
      onNotFound={() => console.log('no results')}
      query={{
        key: 'AIzaSyAlsfF4RwjBRBxaoFMJKqCZu15Z26BvTfk',
        language: 'es',
      }}
      styles={{
        container: {
          flex: 0,
        },
        textInputContainer: {
          width: '100%',
        },
        description: {
          fontWeight: 'bold',
        },
      }}
      nearbyPlacesAPI="GooglePlacesSearch"
      debounce={200}
    />
  );
};
export default MapInput;
