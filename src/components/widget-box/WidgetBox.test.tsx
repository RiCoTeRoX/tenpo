import React from 'react';
import renderer from 'react-test-renderer';
import WidgetBox from './WidgetBox';

describe('Jest Snapshot testing suite', () => {
  it('Matches DOM Snapshot', () => {
    const widgetBox = renderer.create(<WidgetBox title="titulo" />).toJSON();
    expect(widgetBox).toMatchSnapshot();
  });
});
