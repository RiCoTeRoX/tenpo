import React, {FC, PropsWithChildren} from 'react';
import styled from 'styled-components/native';

const WidgetBox: FC<PropsWithChildren<{title: string}>> = ({
  children,
  title,
}) => {
  const Section = styled.View({
    marginBottom: 30,
  });

  const Title = styled.Text({
    fontWeight: 600,
    color: '#000',
    marginBottom: 10,
  });

  const Container = styled.View({
    flexDirection: 'row',
    justifyContent: 'space-between',
  });

  return (
    <Section>
      <Title>{title}</Title>
      <Container>{children}</Container>
    </Section>
  );
};

export default WidgetBox;
