import React, {FC, PropsWithChildren} from 'react';
import styled from 'styled-components/native';

const Container = styled.View(({widescreen}: {widescreen: boolean}) => ({
  backgroundColor: '#068e7f',
  width: widescreen ? 200 : 100,
  borderRadius: 30,
  height: 100,
}));

const Box: FC<PropsWithChildren<{widescreen?: boolean}>> = ({
  children,
  widescreen,
}) => {
  return <Container widescreen={widescreen}>{children}</Container>;
};

export default Box;
