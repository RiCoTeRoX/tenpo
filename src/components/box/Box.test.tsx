import React from 'react';
import renderer from 'react-test-renderer';
import Box from './Box';

describe('Jest Snapshot testing suite', () => {
  it('Matches DOM Snapshot', () => {
    const box = renderer.create(<Box />).toJSON();
    expect(box).toMatchSnapshot();
  });
});
