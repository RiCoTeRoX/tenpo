import React from 'react';
import renderer from 'react-test-renderer';
import MapScreen from './MapScreen';

describe('Jest Snapshot testing suite', () => {
  it('Matches DOM Snapshot', () => {
    const mapScreen = renderer.create(<MapScreen />).toJSON();
    expect(mapScreen).toMatchSnapshot();
  });
});
