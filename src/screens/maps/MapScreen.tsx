import {StyleSheet, View} from 'react-native';
import React, {useCallback, useState} from 'react';
import MapView, {Marker, PROVIDER_GOOGLE, Region} from 'react-native-maps';
import styled from 'styled-components/native';
import {MapInput} from '../../components';
import {Point} from 'react-native-google-places-autocomplete';

const MapsScreen = () => {
  const [region, setRegion] = useState<Region>({
    latitude: -34.6098967315094,
    longitude: -58.39244189563983,
    latitudeDelta: 0.0922,
    longitudeDelta: 0.0421,
  });

  const handleLocationChange = useCallback((location: Point | undefined) => {
    if (location) {
      setRegion({
        latitude: location.lat,
        longitude: location.lng,
        latitudeDelta: 0.009,
        longitudeDelta: 0.009,
      });
    }
  }, []);

  return (
    <View style={styles.container}>
      <View style={styles.input}>
        <MapInput onLocationChange={handleLocationChange} />
      </View>

      <MapView region={region} provider={PROVIDER_GOOGLE} style={styles.map}>
        <Marker
          coordinate={{
            latitude: region.latitude,
            longitude: region.longitude,
          }}
          centerOffset={{x: -18, y: -60}}
          anchor={{x: 0.69, y: 1}}
          title="Lugar"
        />
      </MapView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  input: {
    position: 'absolute',
    zIndex: 2,
    width: '100%',
    margin: 5,
    backgroundColor: 'white',
  },
  mapContainer: {
    backgroundColor: 'green',
  },
  map: {
    backgroundColor: 'gray',
    flex: 1,
  },
});

export default MapsScreen;
