import React from 'react';
import renderer from 'react-test-renderer';
import Home from './Home';

describe('Jest Snapshot testing suite', () => {
  it('Matches DOM Snapshot', () => {
    const home = renderer.create(<Home navigation={{}} />).toJSON();
    expect(home).toMatchSnapshot();
  });
});
