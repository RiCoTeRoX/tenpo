import React, {useCallback} from 'react';
import styled from 'styled-components/native';
import IMAGE_AVATAR from '../../components/images/girl.png';
import IMAGE_SEARCH from '../../components/images/search.png';
import IMAGE_CAROUSEL from '../../components/images/carousel.png';
import {ScreenLayout, Box, WidgetBox, Widgets} from '../../components';
import {TouchableWithoutFeedback} from 'react-native';
import {makeRequest} from '../../api';

const Home = ({navigation}) => {
  const HeaderComp = styled.View({
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 20,
  });

  const Carousel = styled.Image({
    height: 170,
    width: 400,
  });

  const Address = styled.View({
    justifyContent: 'center',
    alignContent: 'center',
    bordertopRightRadius: 30,
    bordertopLeftRadius: 30,
    alignItems: 'center',
    paddingVertical: 20,
  });

  const AddressTitle = styled.Text({
    color: '#068e7f',
  });

  const IconHeader = styled.Image({
    width: 40,
    height: 40,
  });

  const BottomContainer = styled.View({
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    backgroundColor: '#d4f9f5',
  });

  const restaurantes = [
    {title: 'urbano', image: '', rate: 4.5, time: '10-60 min', id: 1},
    {title: 'mc donalds', image: '', rate: 4.5, time: '10-60 min', id: 2},
    {title: 'Melt', image: '', rate: 4.5, time: '10-60 min', id: 3},
  ];

  const handleNavigateButton = useCallback(() => {
    makeRequest()
      .then(() => {
        navigation.navigate('Maps');
      })
      .catch(() => {
        navigation.navigate('InfoModal');
      });
  }, [navigation]);

  return (
    <ScreenLayout>
      <HeaderComp>
        <IconHeader source={IMAGE_AVATAR} resizeMode="contain" />
        <IconHeader source={IMAGE_SEARCH} resizeMode="contain" />
      </HeaderComp>
      <Carousel source={IMAGE_CAROUSEL} resizeMode="contain" />
      <BottomContainer>
        <TouchableWithoutFeedback onPress={handleNavigateButton}>
          <Address>
            <AddressTitle>📍 Agregar direccion de entrega</AddressTitle>
          </Address>
        </TouchableWithoutFeedback>
        <Widgets>
          <WidgetBox title="Restaurantes">
            {restaurantes.map(restaurante => (
              <Box key={restaurante.id} />
            ))}
          </WidgetBox>
          <WidgetBox title="Categorias">
            {restaurantes.map(restaurante => (
              <Box widescreen key={restaurante.id} />
            ))}
          </WidgetBox>
        </Widgets>
      </BottomContainer>
    </ScreenLayout>
  );
};

export default Home;
